
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sapon
 */
public class InsertCompany {

    public static void main(String[] args) {
        Connection cnt = null;
        String dbName = "user.db";
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            cnt = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            cnt.setAutoCommit(false);
            stmt = cnt.createStatement();
            String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (1, 'Admin', 'PASSWORD');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (2, 'Ness', 'PASSWORD');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (3, 'User', 'PASSWORD');";
            stmt.executeUpdate(sql);
            cnt.commit();
            stmt.close();
            cnt.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No library org.sqlite.JDBC");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connect database!!!");
            System.exit(0);
        }
    }

}

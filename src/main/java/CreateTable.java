
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sapon
 */
public class CreateTable {

    public static void main(String[] args) {
        Connection cnt = null;
        String dbName = "user.db";
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            cnt = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            stmt = cnt.createStatement();
            String sql = "CREATE TABLE user (\n"
                    + "    id       INTEGER    PRIMARY KEY ASC AUTOINCREMENT,\n"
                    + "    username CHAR (16)  UNIQUE,\n"
                    + "    password CHAR (255) \n"
                    + ");";
            stmt.executeUpdate(sql);
            stmt.close();
            cnt.close();

        } catch (ClassNotFoundException ex) {
            System.out.println("No library org.sqlite.JDBC");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connect database!!!");
            System.exit(0);
        }
    }
}
